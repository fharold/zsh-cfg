#!/bin/bash
apt-get update
apt-get upgrade
apt-get dist-upgrade
apt-get dist-update
apt-get install chromium tlp cpu-checker qemu-kvm libvirt-bin ubuntu-vm-builder bridge-utils ia32-libs-multiarch emacs clementine git kicad hplip hplip-gui htop gparted pulseaudio-module-bluetooth extra-xdg-menus transmission povray curl gcc g++ make filezilla zsh fonts-powerline sakura
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
chsh -s `which zsh`
git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k
git clone https://github.com/zsh-users/zsh-autosuggestions $ZSH_CUSTOM/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM}:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.0.0/Hack.zip
mkdir hack
unzip Hack.zip -d hack
rm -rf /usr/share/fonts/truetype/hack
mv hack /usr/share/fonts/truetype
fc-cache -f -v
rm ~/.zshrc
cp zshrc ~/.zshrc
source .zshrc
